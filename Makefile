CC = gcc
CFLAGS = -std=c99 -Wall -Wconversion -Wno-sign-conversion -Werror -pedantic
VALFLAGS = --leak-check=full --track-origins=yes --show-reachable=yes
EXEC = pruebas
ZIPNAME = lista_entrega.zip

build: main.c lista.c lista.h pruebas_alumno.c testing.c testing.h
	$(CC) $(CFLAGS) -o $(EXEC) *.c

run: build
	./$(EXEC)

val: build
	valgrind $(VALFLAGS) ./$(EXEC)

clean:
	rm -f *.o $(EXEC) $(ZIPNAME)

zip:
	zip $(ZIPNAME) lista.c pruebas_alumno.c Makefile




