#ifndef LISTA_H
#define LISTA_H

#include <stdlib.h>
#include <stdbool.h>

/* ******************************************************************
 *                DEFINICION DE LOS TIPOS DE DATOS
 * *****************************************************************/

struct lista;
typedef struct lista lista_t;
struct lista_iter;
typedef struct lista_iter lista_iter_t;

/* ******************************************************************
 *                    PRIMITIVAS DE LA LISTA
 * *****************************************************************/

// Crea una lista.
// Post: devuelve una lista vacía.
lista_t *lista_crear(void);

// Devuelve verdadero o falso, según si la lista tiene o no elementos.
// Pre: la lista fue creada.
bool lista_esta_vacia(const lista_t *lista);

// Inserta un nuevo elemento en la primera posición de la lista. Devuelve falso
// en caso de error.
// Pre: la lista fue creada.
// Post: agregó un nuevo elemento al comienzo de la lista.
bool lista_insertar_primero(lista_t *lista, void *dato);

// Inserta un nuevo elemento al final de la lista. Devuelve falso en caso de
// error.
// Pre: la lista fue creada.
// Post: agregó un nuevo elemento al final de la lista.
bool lista_insertar_ultimo(lista_t *lista, void *dato);

// Devuelve el primer elemento y lo elimina de la lista. Si la lista está vacía
// devuelve NULL.
// Pre: la lista fue creada
// Post: se devolvió el primer elemento y se lo eliminó de la lista.
void *lista_borrar_primero(lista_t *lista);

// Devuelve el primer elemento de la lista. Si la lista está vacía devuelve NULL.
// Pre: la lista fue creada.
// Post: se devolvió el primer elemento de la lista, cuando no está vacía.
// Post: no se modificó el contenido de la lista.
void *lista_ver_primero(const lista_t *lista);

// Devuelve el último elemento de la lista. Si la lista está vacía devuelve NULL.
// Pre: la lista fue creada.
// Post: se devolvió el último elemento de la lista, cuando no está vacía.
// Post: no se modificó el contenido de la lista.
void *lista_ver_ultimo(const lista_t *lista);

// Devuelve la cantidad de elementos de la lista. Si la lista está vacía,
// devuelve cero.
// Pre: la lista fue creada.
// Post: se devolvió la cantidad de elementos de la lista.
size_t lista_largo(const lista_t *lista);

// Destruye la lista. Si se recibe la función destruir_dato por parámetro,
// para cada uno de los elementos de la lista, llama a destruir_dato.
// Pre: la lista fue creada. destruir_dato es una función capaz de destruir
// los datos de la lista, o NULL en caso de que no se la utilice.
// Post: se eliminaron todos los elementos de la lista.
void lista_destruir(lista_t *lista, void destruir_dato(void *));

/* ******************************************************************
 *                    PRIMITIVAS DE ITERACION
 * *****************************************************************/

// Crea un iterador para la lista enlazada pasada por parametro.
// Pre: la lista pasada por parametro es una lista enlazada y fue creada.
// Post: devuelve un iterador para la lista enlazada pasada por parametro.
lista_iter_t *lista_iter_crear(lista_t *lista);

// Avanza el iterador una posición, si el iterador no estaba en la última.
// Devuelve true si se pudo avanzar, false si no.
// Pre: el iterador fue creado.
// Post: se avanzó el iterador una posición, si no estaba en la última.
bool lista_iter_avanzar(lista_iter_t *iter);

// Devuelve el elemento correspondiente a la posicion actual del iterador. Si la
// lista asociada al iterador esta vacia, o si el iterador esta al final, devuelve
// NULL.
// Pre: el iterador fue creado.
// Post: se devolvio el elemento correspondiente a la posicion actual del iterador.
// Post: la lista no se modifico.
void *lista_iter_ver_actual(const lista_iter_t *iter);

// Devuelve true o false, segun si el iterador se encuentre o no al final de la
// lista.
// El iterador se encuentra al final de la lista cuando ya ha pasado por todos
// sus elementos (incluyendo el ultimo).
// Pre: el iterador fue creado.
// Se devolvio true si el iterador estaba al final de la lista, y false si no.
bool lista_iter_al_final(const lista_iter_t *iter);

// Destruye el iterador
void lista_iter_destruir(lista_iter_t *iter);

// Inserta el dato pasado por parámetro y devuelve true. Si no se puede insertar,
// devuelve false.
// El nuevo dato se inserta antes de la posición del iterador, y luego de
// insertado, el iterador apunta al nuevo dato (y por lo tanto, su posición relativa
// al comienzo de la lista no varía)
// Ej: l = [1, 2, 3, 4]
//                ^
// inserto 9
//     l = [1, 2, 9, 3, 4]
//                ^
// Pre: el iterador fue creado.
// Post: le insertó el dato en la posición del iteradora.
// Post: la posición del iterador no varía.
bool lista_iter_insertar(lista_iter_t *iter, void *dato);

// Devuelve el elemento correspondiente a la posicion actual del iterador y lo
// elimina de la lista. Si la lista está vacía o si el iterador está al final
// devuelve null.
// Pre: el iterador fue creado.
// Post: devolvió el elemento correspondiente a la posición del iterador y lo
// eliminó de la lista.
// Post: el iterador apunta al que era el siguiente del elemento eliminado.
void *lista_iter_borrar(lista_iter_t *iter);

/* ******************************************************************
 *                    PRIMITIVAS DE ITERADOR INTERNO
 * *****************************************************************/

// Empieza a recorrer la lista desde el principio, y continua mientras que la
// funcion pasada por parámetro aplicada sobre el elemento actual devuelva true.
// El parámetro "extra" que recibe la función es un valor de retorno por interfaz.
// Pre: la lista fue creada.
void lista_iterar(lista_t *lista, bool (*visitar)(void *dato, void *extra), void *extra);

/* *****************************************************************
 *                      PRUEBAS UNITARIAS
 * *****************************************************************/
void pruebas_lista_alumno(void);

#endif // LISTA_H
