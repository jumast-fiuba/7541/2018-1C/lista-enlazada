#include "lista.h"
#include "stdlib.h"
#include "stdbool.h"

typedef struct nodo {
    void* dato;
    struct nodo* siguiente;
} nodo_t;

struct lista {
    nodo_t* primero;
    nodo_t* ultimo;
    size_t largo;
};

struct lista_iter{
    lista_t* lista;
    nodo_t* anterior;
    nodo_t* actual;
};

/* *****************************************************************
 *                    FUNCIONES AUXILIARES
 * *****************************************************************/

static nodo_t* nodo_crear(void *dato){
    nodo_t* nodo = malloc(sizeof(nodo_t));
    if (nodo == NULL){
        return NULL;
    }
    nodo->dato = dato;
    nodo->siguiente = NULL;
    return nodo;
}

/* *****************************************************************
 *                    PRIMITIVAS DE LA LISTA
 * *****************************************************************/

lista_t* lista_crear(void){
    lista_t* lista = malloc(sizeof(lista_t));
    if(lista == NULL){
        return NULL;
    }

    lista->largo = 0;
    lista->primero = NULL;
    lista->ultimo = NULL;
    return lista;
}

bool lista_esta_vacia(const lista_t* lista){
    return lista->largo == 0;
}

bool lista_insertar_primero(lista_t* lista, void* dato){
    nodo_t* nuevo_nodo = nodo_crear(dato);
    if(nuevo_nodo == NULL){
        return false;
    }

    if(lista_esta_vacia(lista)){
        lista->ultimo = nuevo_nodo;
    }

    nuevo_nodo->siguiente = lista->primero;
    lista->primero = nuevo_nodo;
    lista->largo +=1;
    return true;
}

bool lista_insertar_ultimo(lista_t* lista, void*dato){
    nodo_t* nuevo_nodo = nodo_crear(dato);
    if(nuevo_nodo == NULL){
        return false;
    }

    if(lista_esta_vacia(lista)){
        lista->primero = nuevo_nodo;
    }
    else{
        lista->ultimo->siguiente = nuevo_nodo;
    }

    lista->ultimo = nuevo_nodo;
    lista->largo += 1;
    return true;
}

void* lista_borrar_primero(lista_t* lista){
    if(lista_esta_vacia(lista)){
        return NULL;
    }

    nodo_t* nodo_a_borrar = lista->primero;
    if(nodo_a_borrar == lista->ultimo){
        lista->ultimo = NULL;
    }

    lista->primero = lista->primero->siguiente;
    lista->largo -= 1;
    void* dato = nodo_a_borrar->dato;
    free(nodo_a_borrar);
    return dato;
}

void* lista_ver_primero(const lista_t* lista){
    return lista_esta_vacia(lista) ? NULL : lista->primero->dato;
}

void* lista_ver_ultimo(const lista_t* lista){
    return lista_esta_vacia(lista) ? NULL : lista->ultimo->dato;
}

size_t lista_largo(const lista_t* lista){
    return lista->largo;
}

void lista_destruir(lista_t* lista, void destruir_dato(void*)){
    while(!lista_esta_vacia(lista)) {
        void* dato = lista_borrar_primero(lista);
        if(destruir_dato){
            destruir_dato(dato);
        }
    }
    free(lista);
}

/* ******************************************************************
 *                    PRIMITIVAS DE ITERACION
 * *****************************************************************/

lista_iter_t* lista_iter_crear(lista_t* lista){
    lista_iter_t* iter = malloc(sizeof(lista_iter_t));
    if(iter == NULL){
        return NULL;
    }

    iter->lista = lista;
    iter->actual = lista->primero;
    iter->anterior = NULL;
    return iter;
}

bool lista_iter_avanzar(lista_iter_t* iter){
    if(lista_iter_al_final(iter)){
        return false;
    }

    iter->anterior = iter->actual;
    iter->actual = iter->actual->siguiente;
    return true;
}

void* lista_iter_ver_actual(const lista_iter_t* iter){
    return iter->actual == NULL ? NULL : iter->actual->dato;
}

bool lista_iter_al_final(const lista_iter_t* iter){
    return iter->actual == NULL;
}

void lista_iter_destruir(lista_iter_t* iter){
    free(iter);
}

bool lista_iter_insertar(lista_iter_t* iter, void*dato){
    nodo_t* nuevo_nodo = nodo_crear(dato);
    if(nuevo_nodo == NULL){
        return NULL;
    }

    nuevo_nodo->siguiente = iter->actual;
    if(iter->actual == iter->lista->primero){
        iter->lista->primero = nuevo_nodo;
    }
    else{
        iter->anterior->siguiente = nuevo_nodo;
    }
    if(lista_iter_al_final(iter)){
        iter->lista->ultimo = nuevo_nodo;
    }
    iter->actual = nuevo_nodo;
    iter->lista->largo += 1;
    return true;
}

void* lista_iter_borrar(lista_iter_t* iter){
    if(lista_iter_al_final(iter)){
        return NULL;
    }
    else if(iter->actual == iter->lista->primero){
        void* dato = lista_borrar_primero(iter->lista);
        iter->actual = iter->lista->primero;
        return dato;
    }
    else{
        void* dato = iter->actual->dato;
        nodo_t* proximo_actual = iter->actual->siguiente;
        free(iter->actual);
        iter->anterior->siguiente = proximo_actual;
        iter->actual = proximo_actual;
        iter->lista->largo -= 1;
        if(iter->lista->largo == 1){
            iter->lista->ultimo = iter->lista->primero;
        }
        return dato;
    }
}

/* ******************************************************************
 *             PRIMITIVAS DE ITERADOR INTERNO
 * *****************************************************************/

void lista_iterar(lista_t *lista, bool (*visitar)(void *dato, void *extra), void *extra){
    nodo_t* actual = lista->primero;
    while(actual && visitar(actual->dato, extra)){
            actual = actual->siguiente;
        }
}
