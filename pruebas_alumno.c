#include "lista.h"
#include "testing.h"
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

/* ******************************************************************
 *                   PRUEBAS UNITARIAS ALUMNO
 * *****************************************************************/

/* ******************************************************************
 *          PRUEBAS UNITARIAS - PRIMITIVAS DE LA LISTA
 * *****************************************************************/

void estaVacia_listaRecienCreada_esVerdadero(){
    lista_t* lista = lista_crear();
    print_test("lista creada", lista != NULL);
    print_test("lista vacia", lista_esta_vacia(lista));
    lista_destruir(lista, NULL);
}

void borrarPrimero_listaRecienCreada_esNull(){
    lista_t* lista = lista_crear();
    print_test("lista creada", lista != NULL);
    print_test("borrar_primero() es NULL", lista_borrar_primero(lista) == NULL);
    lista_destruir(lista, NULL);
}

void verPrimero_listaRecienCreada_esNull(){
    lista_t* lista = lista_crear();
    print_test("lista creada", lista != NULL);
    print_test("ver_primero() es NULL", lista_ver_primero(lista) == NULL);
    lista_destruir(lista, NULL);
}

void verUltimo_listaRecienCreada_esNull(){
    lista_t* lista = lista_crear();
    print_test("lista creada", lista != NULL);
    print_test("ver_ultimo() es NULL", lista_ver_ultimo(lista) == NULL);
    lista_destruir(lista, NULL);
}

void largo_listaRecienCreada_esCero(){
    lista_t* lista = lista_crear();
    print_test("lista creada", lista != NULL);
    print_test("largo es cero", lista_largo(lista) == 0);
    lista_destruir(lista, NULL);
}

void prueba_lista_vacia(){
    printf("PRUEBAS UNITARIAS PARA CREAR LISTA\n");
    estaVacia_listaRecienCreada_esVerdadero();
    largo_listaRecienCreada_esCero();
    borrarPrimero_listaRecienCreada_esNull();
    verPrimero_listaRecienCreada_esNull();
    verUltimo_listaRecienCreada_esNull();
}

void prueba_insertarPrimero_unSoloElemento(){
    printf("PRUEBAS INSERTAR PRIMERO - UN SOLO ELEMENTO\n");
    lista_t* lista = lista_crear();
    print_test("lista creada", lista != NULL);

    int valor = 5;
    print_test("inserto 5 en la primera posicion", lista_insertar_primero(lista, &valor) == true);
    print_test("no esta vacia", lista_esta_vacia(lista) == false);
    print_test("ver_primero() es 5", lista_ver_primero(lista) == &valor);
    print_test("ver_ultimo() es 5", lista_ver_ultimo(lista) == &valor);
    print_test("largo es 1", lista_largo(lista) == 1);
    print_test("borrar_primero() es 5", lista_borrar_primero(lista) == &valor);
    print_test("esta vacia", lista_esta_vacia(lista) == true);
    print_test("ver_primero() es NULL", lista_ver_primero(lista) == NULL);
    print_test("ver_ultimo() es NULL", lista_ver_ultimo(lista) == NULL);
    print_test("largo es cero", lista_largo(lista) == 0);
    print_test("borrar_primero() es NULL", lista_borrar_primero(lista) == NULL);

    lista_destruir(lista, NULL);
}

void prueba_insertarPrimero_variosElementos(){
    printf("PRUEBAS INSERTAR PRIMERO - VARIOS ELEMENTOS\n");
    lista_t* lista = lista_crear();
    print_test("lista creada", lista != NULL);

    int valor1 = 1;
    int valor2 = 2;
    int valor3 = 3;
    print_test("inserto 1 en la primera posicion", lista_insertar_primero(lista, &valor1) == true);
    print_test("no esta vacia", lista_esta_vacia(lista) == false);
    print_test("ver_primero() es 1", lista_ver_primero(lista) == &valor1);
    print_test("ver_ultimo() es 1", lista_ver_ultimo(lista) == &valor1);
    print_test("largo es 1", lista_largo(lista) == 1);

    print_test("inserto 2 en la primera posicion", lista_insertar_primero(lista, &valor2) == true);
    print_test("no esta vacia", lista_esta_vacia(lista) == false);
    print_test("ver_primero() es 2", lista_ver_primero(lista) == &valor2);
    print_test("ver_ultimo() es 1", lista_ver_ultimo(lista) == &valor1);
    print_test("largo es 2", lista_largo(lista) == 2);

    print_test("inserto 3 en la primera posicion", lista_insertar_primero(lista, &valor3) == true);
    print_test("no esta vacia", lista_esta_vacia(lista) == false);
    print_test("ver_primero() es 3", lista_ver_primero(lista) == &valor3);
    print_test("ver_ultimo() es 1", lista_ver_ultimo(lista) == &valor1);
    print_test("largo es 3", lista_largo(lista) == 3);

    print_test("borrar_primero() es 3", lista_borrar_primero(lista) == &valor3);
    print_test("no esta vacia", lista_esta_vacia(lista) == false);
    print_test("largo es 2", lista_largo(lista) == 2);
    print_test("ver_primero() es 2", lista_ver_primero(lista) == &valor2);
    print_test("ver_ultimo() es 1", lista_ver_ultimo(lista) == &valor1);

    print_test("borrar_primero() es 2", lista_borrar_primero(lista) == &valor2);
    print_test("no esta vacia", lista_esta_vacia(lista) == false);
    print_test("largo es 1", lista_largo(lista) == 1);
    print_test("ver_primero() es 1", lista_ver_primero(lista) == &valor1);
    print_test("ver_ultimo() es 1", lista_ver_ultimo(lista) == &valor1);

    print_test("borrar_primero() es 1", lista_borrar_primero(lista) == &valor1);
    print_test("esta vacia", lista_esta_vacia(lista) == true);
    print_test("largo es 0", lista_largo(lista) == 0);
    print_test("ver_primero() es NULL", lista_ver_primero(lista) == NULL);
    print_test("ver_ultimo() es NULL", lista_ver_ultimo(lista) == NULL);

    lista_destruir(lista, NULL);
}

void prueba_insertarUltimo_unSoloElemento(){
    printf("PRUEBAS INSERTAR ULTIMO - UN SOLO ELEMENTO\n");

    lista_t* lista = lista_crear();
    print_test("lista creada", lista != NULL);

    int valor = 5;
    print_test("inserto 5 en la ultima posicion", lista_insertar_ultimo(lista, &valor) == true);
    print_test("no esta vacia", lista_esta_vacia(lista) == false);
    print_test("ver_primero() es 5", lista_ver_primero(lista) == &valor);
    print_test("ver_ultimo() es 5", lista_ver_ultimo(lista) == &valor);
    print_test("largo es 1", lista_largo(lista) == 1);
    print_test("borrar_primero() es 5", lista_borrar_primero(lista) == &valor);
    print_test("esta vacia", lista_esta_vacia(lista) == true);
    print_test("ver_primero() es NULL", lista_ver_primero(lista) == NULL);
    print_test("ver_ultimo() es NULL", lista_ver_ultimo(lista) == NULL);
    print_test("largo es cero", lista_largo(lista) == 0);
    print_test("borrar_primero() es NULL", lista_borrar_primero(lista) == NULL);

    lista_destruir(lista, NULL);
}

void prueba_insertarUltimo_variosElementos(){
    printf("PRUEBAS INSERTAR ULTIMO - VARIOS ELEMENTOS\n");

    lista_t* lista = lista_crear();
    print_test("lista creada", lista != NULL);

    int valor1 = 1;
    int valor2 = 2;
    int valor3 = 3;
    print_test("inserto 1 en la ultima posicion", lista_insertar_ultimo(lista, &valor1) == true);
    print_test("no esta vacia", lista_esta_vacia(lista) == false);
    print_test("ver_primero() es 1", lista_ver_primero(lista) == &valor1);
    print_test("ver_ultimo() es 1", lista_ver_ultimo(lista) == &valor1);
    print_test("largo es 1", lista_largo(lista) == 1);

    print_test("inserto 2 en la ultima posicion", lista_insertar_ultimo(lista, &valor2) == true);
    print_test("no esta vacia", lista_esta_vacia(lista) == false);
    print_test("ver_primero() es 1", lista_ver_primero(lista) == &valor1);
    print_test("ver_ultimo() es 2", lista_ver_ultimo(lista) == &valor2);
    print_test("largo es 2", lista_largo(lista) == 2);

    print_test("inserto 3 en la ultima posicion", lista_insertar_ultimo(lista, &valor3) == true);
    print_test("no esta vacia", lista_esta_vacia(lista) == false);
    print_test("ver_primero() es 1", lista_ver_primero(lista) == &valor1);
    print_test("ver_ultimo() es 3", lista_ver_ultimo(lista) == &valor3);
    print_test("largo es 3", lista_largo(lista) == 3);

    print_test("borrar_primero() es 1", lista_borrar_primero(lista) == &valor1);
    print_test("no esta vacia", lista_esta_vacia(lista) == false);
    print_test("largo es 2", lista_largo(lista) == 2);
    print_test("ver_primero() es 2", lista_ver_primero(lista) == &valor2);
    print_test("ver_ultimo() es 3", lista_ver_ultimo(lista) == &valor3);

    print_test("borrar_primero() es 2", lista_borrar_primero(lista) == &valor2);
    print_test("no esta vacia", lista_esta_vacia(lista) == false);
    print_test("largo es 1", lista_largo(lista) == 1);
    print_test("ver_primero() es 3", lista_ver_primero(lista) == &valor3);
    print_test("ver_ultimo() es 3", lista_ver_ultimo(lista) == &valor3);

    print_test("borrar_primero() es 3", lista_borrar_primero(lista) == &valor3);
    print_test("esta vacia", lista_esta_vacia(lista) == true);
    print_test("largo es 0", lista_largo(lista) == 0);
    print_test("ver_primero() es NULL", lista_ver_primero(lista) == NULL);
    print_test("ver_ultimo() es NULL", lista_ver_ultimo(lista) == NULL);

    lista_destruir(lista, NULL);

}

void prueba_borrarPrimeroVariasVeces_colaVacia(){
    printf("PRUEBAS BORRAR PRIMERO VARIAS VECES EN LISTA VACIA\n");

    lista_t* lista = lista_crear();
    print_test("lista creada", lista != NULL);

    /* Llamo borrar_primero() varias veces seguidas */
    print_test("borrar_primero() primera vez", lista_borrar_primero(lista) == NULL);
    print_test("borrar_primero() segunda vez", lista_borrar_primero(lista) == NULL);
    print_test("borrar_primero() tercera vez", lista_borrar_primero(lista) == NULL);

    /*  */
    print_test("esta vacia", lista_esta_vacia(lista) == true);
    print_test("ver_primero() es NULL", lista_ver_primero(lista) == NULL);
    print_test("ver_ultimo() es NULL", lista_ver_ultimo(lista) == NULL);
    print_test("largo es cero", lista_largo(lista) == 0);
    print_test("borrar_primero() es NULL", lista_borrar_primero(lista) == NULL);

    /*  */
    int valor = 5;
    print_test("inserto 5 en la primera posicion", lista_insertar_primero(lista, &valor) == true);
    print_test("no esta vacia", lista_esta_vacia(lista) == false);
    print_test("ver_primero() es 5", lista_ver_primero(lista) == &valor);
    print_test("ver_ultimo() es 5", lista_ver_ultimo(lista) == &valor);
    print_test("largo es 1", lista_largo(lista) == 1);
    print_test("borrar_primero() es 5", lista_borrar_primero(lista) == &valor);
    print_test("esta vacia", lista_esta_vacia(lista) == true);
    print_test("ver_primero() es NULL", lista_ver_primero(lista) == NULL);
    print_test("ver_ultimo() es NULL", lista_ver_ultimo(lista) == NULL);
    print_test("largo es cero", lista_largo(lista) == 0);
    print_test("borrar_primero() es NULL", lista_borrar_primero(lista) == NULL);

    lista_destruir(lista, NULL);
}

void prueba_destruir_listaNoVacia_stack(){
    printf("PRUEBAS PARA DESTRUIR LISTA NO VACIA - STACK\n");

    lista_t* lista = lista_crear();
    print_test("lista creada", lista != NULL);

    /* Agrego valores */
    int valor1 = 1;
    int valor2 = 2;
    int valor3 = 3;
    int valor4 = 4;
    int valor5 = 5;
    int valor6 = 6;
    int valor7 = 7;
    int valor8 = 8;

    print_test("inserto 5 en la primera posicion", lista_insertar_primero(lista, &valor5) == true);
    print_test("inserto 4 en la primera posicion", lista_insertar_primero(lista, &valor4) == true);
    print_test("inserto 3 en la primera posicion", lista_insertar_primero(lista, &valor3) == true);
    print_test("inserto 2 en la primera posicion", lista_insertar_primero(lista, &valor2) == true);
    print_test("inserto 1 en la primera posicion", lista_insertar_primero(lista, &valor1) == true);
    print_test("inserto 6 en la ultima posicion", lista_insertar_ultimo(lista, &valor6) == true);
    print_test("inserto 7 en la ultima posicion", lista_insertar_ultimo(lista, &valor7) == true);
    print_test("inserto 8 en la ultima posicion", lista_insertar_ultimo(lista, &valor8) == true);

    /* Borro algunos valores */
    print_test("borro el 1", lista_borrar_primero(lista) == &valor1);
    print_test("borro el 2", lista_borrar_primero(lista) == &valor2);
    print_test("borro el 3", lista_borrar_primero(lista) == &valor3);
    print_test("no esta vacia", lista_esta_vacia(lista) == false);

    lista_destruir(lista, NULL);
    print_test("se destruyo la lista", true);

}

void prueba_destruirListaNoVacia_heap(){
    printf("PRUEBAS PARA DESTRUIR LISTA NO VACIA - HEAP\n");

    const size_t cant_elementos = 1000;

    /* Creo la lista */
    lista_t* lista = lista_crear();

    /* Verifico que se haya creado correctamente */
    print_test("lista creada", lista != NULL);

    /* Inserto enteros alojados en el heap */
    size_t i;
    for(i = 1; i <= cant_elementos; i++){
        int* valor = malloc(sizeof(valor));
        lista_insertar_ultimo(lista, valor);
    }
    /* Verifico que se hayan insertado todos los valores */
    print_test("se insertaron en memoria dinamica 1000 enteros", i == cant_elementos + 1);

    /* Borro algunos de los enteros insertados */
    // No los borro todos para que queden elementos en la lista cuando se
    // la destruya.
    size_t cant_elementos_a_borrar = cant_elementos / 2;
    size_t j;
    for(j = 1; j <= cant_elementos_a_borrar; j++){
        int* borrado = lista_borrar_primero(lista);
        free(borrado);
   }
    print_test("se borraron 500 enteros", j == 501);
    /* Verifico que la lista no esté vacía */
    print_test("la lista no está vacía", lista_esta_vacia(lista) == false);

    /* Destruyo la lista */
    lista_destruir(lista, free);
    print_test("se destruyo la lista", true);
}


void pruebas_primitivas_lista(){
    prueba_lista_vacia();
    prueba_insertarPrimero_unSoloElemento();
    prueba_insertarPrimero_variosElementos();
    prueba_insertarUltimo_unSoloElemento();
    prueba_insertarUltimo_variosElementos();
    prueba_borrarPrimeroVariasVeces_colaVacia();
    prueba_destruir_listaNoVacia_stack();
    prueba_destruirListaNoVacia_heap();
}

/* ******************************************************************
 *          PRUEBAS UNITARIAS - PRIMITIVAS DE ITERACION
 * *****************************************************************/

void pruebas_primitivasIteracion_listaVacia(){
    printf("PRUEBAS PRIMITIVAS DE ITERACION - LISTA VACIA\n");

    lista_t* lista = lista_crear();
    print_test("lista creada", lista != NULL);

    lista_iter_t* iter = lista_iter_crear(lista);
    print_test("iterador creado", iter != NULL);

    print_test("ver_actual() es NULL", lista_iter_ver_actual(iter) == NULL);
    print_test("al_final() es true", lista_iter_al_final(iter) == true);
    print_test("avanzar() es false", lista_iter_avanzar(iter) == false);
    print_test("ver_actual() es NULL", lista_iter_ver_actual(iter) == NULL);
    print_test("al_final() es true", lista_iter_al_final(iter) == true);


    lista_destruir(lista, NULL);
    lista_iter_destruir(iter);
}

void pruebas_primitivasIteracion_listaConUnElemento(){
    printf("PRUEBAS PRIMITIVAS DE ITERACION - LISTA CON UN ELEMENTO\n");

    lista_t* lista = lista_crear();
    print_test("lista creada", lista != NULL);

    /* Inserto un elemento */
    int valor = 5;
    print_test("inserto 5 en la primera posicion", lista_insertar_primero(lista, &valor));

    lista_iter_t* iter = lista_iter_crear(lista);
    print_test("iterador creado", iter != NULL);

    print_test("ver_actual() es 5", lista_iter_ver_actual(iter) == &valor);
    print_test("al_final() es false", lista_iter_al_final(iter) == false);
    print_test("avanzar() es true", lista_iter_avanzar(iter) == true);
    print_test("ver_actual() es NULL", lista_iter_ver_actual(iter) == NULL);
    print_test("al_final() es true", lista_iter_al_final(iter) == true);
    print_test("avanzar() es false", lista_iter_avanzar(iter) == false);

    lista_destruir(lista, NULL);
    lista_iter_destruir(iter);
}

void pruebas_primitivasIteracion_listaConVariosElementos(){
    printf("PRUEBAS PRIMITIVAS DE ITERACION - LISTA CON VARIOS ELEMENTOS\n");

    lista_t* lista = lista_crear();
    print_test("lista creada", lista != NULL);

    int valor1 = 1;
    int valor2 = 2;
    int valor3 = 3;
    int valor4 = 4;
    print_test("inserto 1 en la ultima posicion", lista_insertar_ultimo(lista, &valor1));
    print_test("inserto 2 en la ultima posicion", lista_insertar_ultimo(lista, &valor2));
    print_test("inserto 3 en la ultima posicion", lista_insertar_ultimo(lista, &valor3));
    print_test("inserto 4 en la ultima posicion", lista_insertar_ultimo(lista, &valor4));

    lista_iter_t* iter = lista_iter_crear(lista);
    print_test("iterador creado", iter != NULL);

    print_test("ver_actual() es 1", lista_iter_ver_actual(iter) == &valor1);
    print_test("al_final() es false", lista_iter_al_final(iter) == false);
    print_test("avanzar() es true", lista_iter_avanzar(iter) == true);

    print_test("ver_actual() es 2", lista_iter_ver_actual(iter) == &valor2);
    print_test("al_final() es false", lista_iter_al_final(iter) == false);
    print_test("avanzar() es true", lista_iter_avanzar(iter) == true);

    print_test("ver_actual() es 3", lista_iter_ver_actual(iter) == &valor3);
    print_test("al_final() es false", lista_iter_al_final(iter) == false);
    print_test("avanzar() es true", lista_iter_avanzar(iter) == true);

    print_test("ver_actual() es 4", lista_iter_ver_actual(iter) == &valor4);
    print_test("al_final() es false", lista_iter_al_final(iter) == false);
    print_test("avanzar() es true", lista_iter_avanzar(iter) == true);

    print_test("ver_actual() es NULL", lista_iter_ver_actual(iter) == NULL);
    print_test("al_final() es true", lista_iter_al_final(iter) == true);
    print_test("avanzar() es false", lista_iter_avanzar(iter) == false);


    lista_destruir(lista, NULL);
    lista_iter_destruir(iter);
}

void pruebas_primitivasIteracion_insertarSinAvanzarIterador(){
    printf("PRUEBAS LISTA CON ITERADOR - INSERTAR SIN AVANZAR ITERADOR\n");

    lista_t* lista = lista_crear();
    print_test("lista creada", lista != NULL);

    lista_iter_t* iter = lista_iter_crear(lista);
    print_test("iterador creado", iter != NULL);

    /* Verifico propiedades de la lista */
    print_test("esta vacia", lista_esta_vacia(lista) == true);
    print_test("largo es cero", lista_largo(lista) == 0);
    print_test("ver_primero() es NULL", lista_ver_primero(lista) == NULL);
    print_test("ver_ultimo() es NULL", lista_ver_ultimo(lista) == NULL);

    /* Inserto un elemento */
    int valor1 = 1;
    print_test("inserto 1", lista_iter_insertar(iter, &valor1) == true);

    /* Verifico propiedades de la lista */
    print_test("no esta vacia", lista_esta_vacia(lista) == false);
    print_test("largo es 1", lista_largo(lista) == 1);
    print_test("ver_primero() es 1", lista_ver_primero(lista) == &valor1);
    print_test("ver_ultimo() es 1", lista_ver_ultimo(lista) == &valor1);

    /* Inserto mas elementos */
    int valor2 = 2;
    int valor3 = 3;
    print_test("inserto 3", lista_iter_insertar(iter, &valor3));
    print_test("inserto 2", lista_iter_insertar(iter, &valor2));
    print_test("inserto 1", lista_iter_insertar(iter, &valor1));
    // lista = [1, 2, 3, 1]
    print_test("borrar_primero() es 1", lista_borrar_primero(lista) == &valor1);
    print_test("ver_primero() es 2", lista_ver_primero(lista) == &valor2);
    print_test("ver_ultimo() es 1", lista_ver_ultimo(lista) == &valor1);
    print_test("borrar_primero() es 2", lista_borrar_primero(lista) == &valor2);
    print_test("ver_primero() es 3", lista_ver_primero(lista) == &valor3);
    print_test("ver_ultimo() es 1", lista_ver_ultimo(lista) == &valor1);
    print_test("borrar_primero() es 3", lista_borrar_primero(lista) == &valor3);
    print_test("ver_primero() es 1", lista_ver_primero(lista) == &valor1);
    print_test("ver_ultimo() es 1", lista_ver_ultimo(lista) == &valor1);
    print_test("borrar_primero() es 1", lista_borrar_primero(lista) == &valor1);
    print_test("esta_vacia", lista_esta_vacia(lista) == true);
    print_test("largo es 0", lista_largo(lista) == 0);
    print_test("ver_primero() es NULL", lista_ver_primero(lista) == NULL);
    print_test("ver_ultimo() es NULL", lista_ver_ultimo(lista) == NULL);

    lista_destruir(lista, NULL);
    lista_iter_destruir(iter);
}

void pruebas_primitivasIteracion_insertarAvanzandoIterador(){
    printf("PRUEBAS LISTA CON ITERADOR - INSERTAR AVANZANDO ITERADOR\n");

    lista_t* lista = lista_crear();
    print_test("lista creada", lista != NULL);

    lista_iter_t* iter = lista_iter_crear(lista);
    print_test("iterador creado", iter != NULL);

    /* Verifico propiedades de la lista */
    print_test("esta vacia", lista_esta_vacia(lista) == true);
    print_test("largo es cero", lista_largo(lista) == 0);
    print_test("ver_primero() es NULL", lista_ver_primero(lista) == NULL);
    print_test("ver_ultimo() es NULL", lista_ver_ultimo(lista) == NULL);

    /* Inserto varios elementos */
    int valor1 = 1;
    int valor2 = 2;
    int valor3 = 3;
    int valor4 = 4;
    int valor5 = 5;
    int valor6 = 6;
    int valor7 = 7;
    int valor8 = 8;
    int valor9 = 9;
    int valor10 = 10;
    print_test("inserto 5", lista_iter_insertar(iter, &valor5) == true);
    print_test("inserto 4", lista_iter_insertar(iter, &valor4) == true);
    print_test("inserto 3", lista_iter_insertar(iter, &valor3) == true);
    print_test("inserto 2", lista_iter_insertar(iter, &valor2) == true);
    print_test("inserto 1", lista_iter_insertar(iter, &valor1) == true);
    // lista = [1, 2 ,3 ,4 ,5]
    print_test("ver actual es 1", lista_iter_ver_actual(iter) == &valor1);
    print_test("avanza el iterador", lista_iter_avanzar(iter) == true);
    print_test("ver actual es 2", lista_iter_ver_actual(iter) == &valor2);
    print_test("avanza el iterador", lista_iter_avanzar(iter) == true);
    print_test("ver actual es 3", lista_iter_ver_actual(iter) == &valor3);
    print_test("inserto 6", lista_iter_insertar(iter, &valor6) == true);
    print_test("ver_actual() es 6", lista_iter_ver_actual(iter) == &valor6);
    print_test("avanza el iterador", lista_iter_avanzar(iter) == true);
    print_test("ver_actual() es 3", lista_iter_ver_actual(iter) == &valor3);
    print_test("avanza el iterador", lista_iter_avanzar(iter) == true);
    print_test("ver_actual() es 4", lista_iter_ver_actual(iter) == &valor4);
    print_test("inserto 7", lista_iter_insertar(iter, &valor7) == true);
    print_test("ver_actual() es 7", lista_iter_ver_actual(iter) == &valor7);
    print_test("avanza el iterador", lista_iter_avanzar(iter) == true);
    print_test("ver_actual() es 4", lista_iter_ver_actual(iter) == &valor4);
    print_test("avanza el iterador", lista_iter_avanzar(iter) == true);
    print_test("ver_actual() es 5", lista_iter_ver_actual(iter) == &valor5);
    print_test("inserto 8", lista_iter_insertar(iter, &valor8) == true);
    print_test("ver_actual() es 8", lista_iter_ver_actual(iter) == &valor8);
    print_test("avanza el iterador", lista_iter_avanzar(iter) == true);
    print_test("ver_actual() es 5", lista_iter_ver_actual(iter) == &valor5);
    print_test("avanza el iterador", lista_iter_avanzar(iter) == true);
    print_test("avanza el iterador es false", lista_iter_avanzar(iter) == false);
    print_test("iterador al final", lista_iter_al_final(iter) == true);
    print_test("avanza el iterador es false", lista_iter_avanzar(iter) == false);
    print_test("inserto 10", lista_iter_insertar(iter, &valor10) == true);
    print_test("ver_actual() es 10", lista_iter_ver_actual(iter) == &valor10);
    print_test("inserto 9", lista_iter_insertar(iter, &valor9) == true);
    print_test("ver_actual() es 9", lista_iter_ver_actual(iter) == &valor9);
    print_test("avanza el iterador", lista_iter_avanzar(iter) == true);
    print_test("avanza el iterador", lista_iter_avanzar(iter) == true);
    print_test("avanza el iterador es false", lista_iter_avanzar(iter) == false);

    lista_destruir(lista, NULL);
    lista_iter_destruir(iter);
}

void pruebas_primitivasIteracion_borrar(){
    printf("PRUEBAS LISTA CON ITERADOR - BORRAR\n");

    lista_t* lista = lista_crear();
    print_test("lista creada", lista != NULL);

    lista_iter_t* iter = lista_iter_crear(lista);
    print_test("iterador creado", iter != NULL);

    /* Borro de la lista vacia */
    print_test("esta vacia", lista_esta_vacia(lista) == true);
    print_test("borrar de la lista vacia es NULL", lista_iter_borrar(iter) == NULL);

    /* Inserto elementos */
    int valor1 = 1;
    int valor2 = 2;
    int valor3 = 3;
    int valor4 = 4;
    int valor5 = 5;
    print_test("inserto 5", lista_iter_insertar(iter, &valor5) == true);
    print_test("inserto 4", lista_iter_insertar(iter, &valor4) == true);
    print_test("inserto 3", lista_iter_insertar(iter, &valor3) == true);
    print_test("inserto 2", lista_iter_insertar(iter, &valor2) == true);
    print_test("inserto 1", lista_iter_insertar(iter, &valor1) == true);

    /* Empiezo a borrar */
    print_test("borro el 1 (primer elemento)", lista_iter_borrar(iter) == &valor1);
    print_test("largo es 4", lista_largo(lista) == 4);
    print_test("borro el 2 (primer elemento)", lista_iter_borrar(iter) == &valor2);
    print_test("largo es 3", lista_largo(lista) == 3);
    print_test("avanzo el iterador", lista_iter_avanzar(iter) == true);
    print_test("ver actual es 4", lista_iter_ver_actual(iter) == &valor4);
    print_test("borro el 4", lista_iter_borrar(iter) == &valor4);
    print_test("largo es 2", lista_largo(lista) == 2);
    print_test("ver actual es 5", lista_iter_ver_actual(iter) == &valor5);
    print_test("borro el 5", lista_iter_borrar(iter) == &valor5);
    print_test("estoy al final", lista_iter_al_final(iter) == true);
    print_test("borrar es null", lista_iter_borrar(iter) == NULL);
    print_test("largo es 1", lista_largo(lista) == 1);
    print_test("ver primero es 3", lista_ver_primero(lista) == &valor3);

    lista_destruir(lista, NULL);
    lista_iter_destruir(iter);

}

void pruebas_primitivas_iteracion(){
    pruebas_primitivasIteracion_listaVacia();
    pruebas_primitivasIteracion_listaConUnElemento();
    pruebas_primitivasIteracion_listaConVariosElementos();
    pruebas_primitivasIteracion_insertarSinAvanzarIterador();
    pruebas_primitivasIteracion_insertarAvanzandoIterador();
    pruebas_primitivasIteracion_borrar();
}

/* ******************************************************************
 *    PRUEBAS UNITARIAS - PRIMITIVAS DE ITERADOR INTERNO
 * *****************************************************************/

bool suma(void* dato, void* extra){
    int* acum = extra;
    int* valor = dato;
    *acum += *valor;
    return true;
}

bool suma_par(void* dato, void* extra){
    int* acum = extra;
    int* valor = dato;
    if(*valor % 2 == 0){
        *acum += *valor;
    }
    return true;
}

bool suma_si_menor_o_igual_a_4(void* dato, void* extra){
    int* acum = extra;
    int* valor = dato;
    if(*valor <= 4){
        *acum += *valor;
        return true;
    }
    return false;
}

void pruebas_primitivas_iteradorInterno(){
    printf("PRUEBAS DE ITERADOR INTERNO\n");

    lista_t* lista = lista_crear();
    print_test("lista creada", lista != NULL);

    int valor1 = 1;
    int valor2 = 2;
    int valor3 = 3;
    int valor4 = 4;
    int valor5 = 5;
    int valor6 = 6;
    int valor7 = 7;
    print_test("inserto 7", lista_insertar_primero(lista, &valor7) == true);
    print_test("inserto 6", lista_insertar_primero(lista, &valor6) == true);
    print_test("inserto 5", lista_insertar_primero(lista, &valor5) == true);
    print_test("inserto 4", lista_insertar_primero(lista, &valor4) == true);
    print_test("inserto 3", lista_insertar_primero(lista, &valor3) == true);
    print_test("inserto 2", lista_insertar_primero(lista, &valor2) == true);
    print_test("inserto 1", lista_insertar_primero(lista, &valor1) == true);

    /* Sumo todos los números */
    int resultado_suma = 0;
    lista_iterar(lista, suma, &resultado_suma);
    print_test("la suma de todos los numeros es 28", resultado_suma == 28);

    /* Sumo todos los números pares */
    int resultado_suma_pares = 0;
    lista_iterar(lista, suma_par, &resultado_suma_pares);
    print_test("la suma de todos los numeros pares es 12", resultado_suma_pares == 12);

    /* Sumo hasta que aparezca un 4 (incluyendo al 4)  */
    int resultado_suma_hasta_4_inclusive = 0;
    lista_iterar(lista, suma_si_menor_o_igual_a_4, &resultado_suma_hasta_4_inclusive);
    print_test("la suma de todos los numeros hasta el 4 inclusive es 10", resultado_suma_hasta_4_inclusive == 10);


    lista_destruir(lista, NULL);
}

void prueba_lista_borrar(){
    printf("PRUEBA LISTA BORRAR\n");

    lista_t* lista = lista_crear();
    print_test("crear lista", lista != NULL);

    int valor1 = 1;
    int valor2 = 2;
    int valor3 = 3;

    print_test("insertar primero valor1", lista_insertar_primero(lista, &valor1) == true);
    print_test("insertar primero valor2", lista_insertar_primero(lista, &valor2) == true);
    print_test("insertar primero valor3", lista_insertar_primero(lista, &valor3) == true);


    lista_iter_t* iter = lista_iter_crear(lista);
    print_test("crear iterador", iter != NULL);
    print_test("iter no esta al final", lista_iter_al_final(iter) == false);
    print_test("iter borrar es valor3", lista_iter_borrar(iter) == &valor3);
    print_test("iter ver_actual es valor2", lista_iter_ver_actual(iter) == &valor2);
    print_test("lista ver_primero es valor2", lista_ver_primero(lista) == &valor2);
    print_test("iter borrar es valor2", lista_iter_borrar(iter) == &valor2);
    print_test("iter ver_actual es valor1", lista_iter_ver_actual(iter) == &valor1);
    print_test("lista ver_primero es valor1", lista_ver_primero(lista) == &valor1);
    print_test("iter borrar es valor1", lista_iter_borrar(iter) == &valor1);
    print_test("iter borrar es NULL", lista_iter_borrar(iter) == NULL);
    print_test("iter avanzar es false", lista_iter_avanzar(iter) == false);
    print_test("lista esta_vacia", lista_esta_vacia(lista) == true);
    print_test("lista largo es 0", lista_largo(lista) == 0);
    print_test("lista ver_primero es NULL", lista_ver_primero(lista) == NULL);
    print_test("insertar_ultimo valor1", lista_insertar_ultimo(lista, &valor1) == true);
    print_test("insertar_ultimo valor2", lista_insertar_ultimo(lista, &valor2) == true);
    print_test("insertar_ultimo valor3", lista_insertar_ultimo(lista, &valor3) == true);

    lista_iter_t* iter2 = lista_iter_crear(lista);
    print_test("crear iterador", iter2 != NULL);
    print_test("iter no esta al final", lista_iter_al_final(iter2) == false);
    print_test("iter no esta al final", lista_iter_al_final(iter2) == false);
    print_test("iter ver_actual es valor1", lista_iter_ver_actual(iter2) == &valor1);
    print_test("avanzar es true", lista_iter_avanzar(iter2) == true);
    print_test("iter ver_actual es valor2", lista_iter_ver_actual(iter2) == &valor2);
    print_test("iter borrar es valor2", lista_iter_borrar(iter2) == &valor2);
    print_test("lista largo es 2", lista_largo(lista) == 2);
    print_test("iter ver_actual es valor3", lista_iter_ver_actual(iter2) ==&valor3);
    print_test("iter borrar es valor3", lista_iter_borrar(iter2) == &valor3);
    print_test("lista largo es 1", lista_largo(lista) == 1);
    print_test("iter avanzar es false", lista_iter_avanzar(iter2) == false);
    print_test("iter borrar es NULL", lista_iter_borrar(iter2) == NULL);
    print_test("iter ver actual es NULL", lista_iter_ver_actual(iter2) == NULL);
    /* Pruebo que se puede insertar al final luego de haber borrado el ultimo con el iterador */
    print_test("insertar al final luego de borrar el ultimo", lista_insertar_ultimo(lista, &valor2) == true);
    print_test("iter ver actual es NULL", lista_iter_ver_actual(iter2) == NULL);
    print_test("lista largo es 2", lista_largo(lista) == 2);

    lista_iter_t* iter3 = lista_iter_crear(lista);
    print_test("crear iterador", iter3 != NULL);
    print_test("iter no esta al final", lista_iter_al_final(iter3) == false);
    print_test("iter ver_actual es valor1", lista_iter_ver_actual(iter3) == &valor1);
    print_test("iter borrar es valor1", lista_iter_borrar(iter3) == &valor1);
    print_test("iter ver_actual es valor2", lista_iter_ver_actual(iter3) == &valor2);
    print_test("iter borrar es valor2", lista_iter_borrar(iter3) == &valor2);
    print_test("iter ver_actual es NULL", lista_iter_ver_actual(iter3) == NULL);
    print_test("iter borrar es NULL", lista_iter_borrar(iter3) == NULL);
    print_test("iter avanzar es fale", lista_iter_avanzar(iter3) == false);
    print_test("lista esta vacia", lista_esta_vacia(lista) == true);
    print_test("lista largo es 0", lista_largo(lista) == 0);
    print_test("lista ver_primero es NULL", lista_ver_primero(lista) == NULL);


    lista_destruir(lista, NULL);
    lista_iter_destruir(iter);
    lista_iter_destruir(iter2);
    lista_iter_destruir(iter3);
}

void prueba_dosIteradores(){
    printf("PRUEBA DOS ITERADORES\n");

    lista_t* lista = lista_crear();
    print_test("crear lista", lista != NULL);
    print_test("lista esta vacia inicialmente", lista_esta_vacia(lista) == true);

    int valor1 = 1;
    /* Inserto al principio 1000 veces valor1 */
    for (int i = 1; i <= 1000; i++){
        lista_insertar_primero(lista, &valor1);
    }
    print_test("insertar_primero 1000 veces valor1", true);
    print_test("largo es 1000", lista_largo(lista) == 1000);

    int valor2 = 2;
    /* Inserto al final 1000 veces valor2 */
    for (int i = 1; i <= 1000; i++){
        lista_insertar_ultimo(lista, &valor2);
    }
    print_test("insertar_ultimo 1000 veces valor2", true);
    print_test("largo es 2000", lista_largo(lista) == 2000);

    /* Borro el primero 1000 veces si es valor1*/
    for(int i = 1; i <= 1000; i++){
        if(lista_ver_primero(lista) == &valor1){
            lista_borrar_primero(lista);
        }
    }
    print_test("borrar primero 1000 veces valor1", true);
    print_test("largo es 1000", lista_largo(lista) == 1000);

    /* Borro el primero 1000 veces si es valor2 */
    for(int i = 1; i <= 1000; i++){
        if(lista_ver_primero(lista) == &valor2){
            lista_borrar_primero(lista);
        }
    }
    print_test("borrar primero 1000 veces valor2", true);
    print_test("largo es 0", lista_largo(lista) == 0);



    lista_iter_t* iter = lista_iter_crear(lista);
    print_test("crear iterador", iter != NULL);

    int valor3 = 0;
    int valor4 = 4;
    print_test("inserta con el iterador al principio", lista_iter_insertar(iter, &valor3));
    print_test("se cambio el primer elemento de la lista", lista_ver_primero(lista) == &valor3);
    print_test("ver actual con el iterador luego de insertar", lista_iter_ver_actual(iter) == &valor3);
    print_test("el largo es 1 luego de insertar", lista_largo(lista) == 1);
    print_test("no esta al final", lista_iter_al_final(iter) == false);
    print_test("avanza despues de insertar", lista_iter_avanzar(iter) == true);
    print_test("esta al final", lista_iter_al_final(iter) == true);
    print_test("insercion con el iterador al final", lista_iter_insertar(iter, &valor4) == true);
    print_test("verifico actual luego de insertar al final", lista_iter_ver_actual(iter) == &valor4);
    print_test("se actualizo el largo", lista_largo(lista) == 2);

    lista_iter_t* iter2 = lista_iter_crear(lista);
    print_test("creacion de un nuevo iterador", iter2 != NULL);
    print_test("avanzo el segundo iterador", lista_iter_avanzar(iter2) == true);
    print_test("ver actual con el iterador", lista_iter_ver_actual(iter2) == &valor4);
    print_test("avanzo el segundo iterador", lista_iter_avanzar(iter2) == true);
    print_test("el segundo iterador esta al final", lista_iter_al_final(iter2) == true);

    lista_destruir(lista, NULL);
    lista_iter_destruir(iter);
    lista_iter_destruir(iter2);
}

void prueba_lista_insertar(){
    printf("PRUEBA LISTA INSERTAR\n");

    lista_t* lista = lista_crear();
    print_test("crear lista", lista != NULL);

    lista_iter_t* iter = lista_iter_crear(lista);
    print_test("crear iterador", iter != NULL);

    int valor1 = 1;
    print_test("insertar con el iterador al principio", lista_iter_insertar(iter, &valor1));
    print_test("ver_primero es valor1", lista_ver_primero(lista) == &valor1);
    print_test("ver_ultimo es valor1", lista_ver_ultimo(lista) == &valor1);
    print_test("se cambo el primer elemento de la lista", lista_ver_primero(lista) == &valor1);
    print_test("ver actual con el iterador luego de insertar", lista_iter_ver_actual(iter) == &valor1);
    print_test("largo es 1 luego de insertar un elemento", lista_largo(lista) == 1);
    print_test("no esta al final", lista_iter_al_final(iter) == false);
    print_test("avanzar despues de insertar", lista_iter_avanzar(iter) == true);
    print_test("esta al final", lista_iter_al_final(iter) == true);

    int valor2 = 2;
    print_test("insercion con el iterador al final", lista_iter_insertar(iter, &valor2) == true);
    print_test("verifico actual luego de insertar al final", lista_iter_ver_actual(iter) == &valor2);
    print_test("ver_primero es valor1", lista_ver_primero(lista) == &valor1);
    print_test("ver_ultimo es valor2", lista_ver_ultimo(lista) == &valor2);

    int valor3 = 3;
    print_test("insertar_ultimo valor3", lista_insertar_ultimo(lista, &valor3) == true);


    lista_destruir(lista, NULL);
    lista_iter_destruir(iter);
}


void pruebas_lista_alumno(){
    pruebas_primitivas_lista();
    pruebas_primitivas_iteracion();
    pruebas_primitivas_iteradorInterno();
    prueba_lista_borrar();
    prueba_dosIteradores();
    prueba_lista_insertar();
}